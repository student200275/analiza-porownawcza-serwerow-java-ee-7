package pl.edu.pwr.student200275.ejb;

import javax.ejb.Stateful;

@Stateful
public class EjbStateless {
	
	public String helloWorld() {
		return "Hello, world!";
	}
}
