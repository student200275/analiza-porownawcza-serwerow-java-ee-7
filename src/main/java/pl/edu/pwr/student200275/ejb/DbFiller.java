package pl.edu.pwr.student200275.ejb;

import javax.annotation.PostConstruct;
import javax.annotation.sql.DataSourceDefinition;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import pl.edu.pwr.student200275.jpa.Customer;
import pl.edu.pwr.student200275.jpa.Equipment;

@Singleton
@Startup
@DataSourceDefinition(name = "java:app/MyApp/MyDS", className = "org.h2.jdbcx.JdbcDataSource", url = "jdbc:h2:mem:test")
public class DbFiller {

	@PersistenceContext
	private EntityManager em;
	
	@PostConstruct
	public void insert() {
		insertCustomers();
		insertEquipment();
	}
	
	public String helloWorld() {
		Logger.log("DbFiller.helloWorld()." + System.nanoTime());
		return "Hello, world!";
	}
	
	private void insertCustomers() {
		Customer cust = new Customer("Adam Nowak");
		cust.setCity("Warszawa");
		cust.setStreet("Zielona 10a");
		cust.setZipCode("02-202");
		cust.setPhone("123123123");
		em.persist(cust);
		cust = new Customer("Jan Kowalski");
		cust.setCity("Wrocław");
		cust.setStreet("Czerwona 20b");
		cust.setZipCode("52-202");
		cust.setPhone("600600600");
		em.persist(cust);
		cust = new Customer("Agnieszka Nowak");
		cust.setCity("Kraków");
		cust.setStreet("Żółta 30c");
		cust.setZipCode("12-202");
		cust.setPhone("555555555");
		em.persist(cust);		
	}
	
	private void insertEquipment() {
		em.persist(new Equipment("Narty Atomic MX6 - 173cm"));
		Equipment eq = new Equipment("Narty Atomic MX6 - 155cm");
		eq.setAvailable(false);
		em.persist(eq);
		em.persist(new Equipment("Narty Atomic MX6 - 140cm"));
		em.persist(new Equipment("Narty Atomic MX6 - 177cm"));
		em.persist(new Equipment("Narty Atomic MX6 - 190cm"));
		em.persist(new Equipment("Narty Atomic MX6 - 200cm"));
	}
}
