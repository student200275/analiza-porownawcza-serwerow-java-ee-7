package pl.edu.pwr.student200275.ejb;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class Logger {
	private static List<String> logs = new ArrayList<>();
	private static java.util.logging.Logger logger = java.util.logging.Logger.getLogger("master");

	private Logger () {
	}
	
	public static void log(String text) {
		logs.add(text);
//		logger.info(text);
	}
	
	public static void print() {
		String log = logs.stream().collect(Collectors.joining("\n"));
		logger.info(log);
		logs.clear();
	}
}
