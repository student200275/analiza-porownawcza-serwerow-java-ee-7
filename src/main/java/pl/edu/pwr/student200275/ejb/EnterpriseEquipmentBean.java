package pl.edu.pwr.student200275.ejb;

import java.util.List;

import javax.ejb.Singleton;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import pl.edu.pwr.student200275.jpa.Equipment;

@Singleton
public class EnterpriseEquipmentBean {

	@PersistenceContext
	private EntityManager em;

	public List<Equipment> getList() {
		return em.createQuery("from Equipment e order by e.id asc", Equipment.class).getResultList();
	}

	public Equipment get(Long id) {
		return em.find(Equipment.class, id);
	}

	public Equipment add(Equipment equipment) {
		em.persist(equipment);
		return equipment;
	}

	public void update(Equipment equipment) {
		Equipment eq = em.find(Equipment.class, equipment.getId());
		eq.setName(equipment.getName());
	}

	public List<Equipment> getAvailable() {
		return em.createQuery("from Equipment e where e.available=true order by e.id asc", Equipment.class)
				.getResultList();
	}
}
