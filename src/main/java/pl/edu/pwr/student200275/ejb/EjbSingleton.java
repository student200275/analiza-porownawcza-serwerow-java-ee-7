package pl.edu.pwr.student200275.ejb;

import javax.ejb.Singleton;

@Singleton
public class EjbSingleton {
	
	public String helloWorld() {
		return "Hello, world!";
	}
}
