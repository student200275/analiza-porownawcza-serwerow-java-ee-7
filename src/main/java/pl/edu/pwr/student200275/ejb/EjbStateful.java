package pl.edu.pwr.student200275.ejb;

import javax.ejb.Singleton;

@Singleton
public class EjbStateful {
	
	public String helloWorld() {
		return "Hello, world!";
	}
}
