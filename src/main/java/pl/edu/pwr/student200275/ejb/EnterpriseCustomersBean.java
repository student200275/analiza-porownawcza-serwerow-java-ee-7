package pl.edu.pwr.student200275.ejb;

import java.util.Date;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Singleton;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import pl.edu.pwr.student200275.jpa.Customer;
import pl.edu.pwr.student200275.jpa.Equipment;
import pl.edu.pwr.student200275.jpa.Order;

@Singleton
public class EnterpriseCustomersBean {

	@PersistenceContext
	private EntityManager em;

	@EJB
	private EnterpriseEquipmentBean eqEjb;

	public List<Customer> getList() {
		Logger.log("EnterpriseCustomersBean.getList()." + System.nanoTime());
		return em.createQuery("from Customer c left join fetch c.orders o order by c.id asc", Customer.class).getResultList();
	}

	public Customer get(Long id) {
		Logger.log("EnterpriseCustomersBean.get(" + id + ")." + System.nanoTime());
		Customer cust = em.find(Customer.class, id);
		cust.getOrders().size();
		return cust;
	}

	public Customer add(Customer customer) {
		Logger.log("EnterpriseCustomersBean.add(...)." + System.nanoTime());
		em.persist(customer);
		return customer;
	}

	public void update(Customer customer) {
		Logger.log("EnterpriseCustomersBean.update(...)." + System.nanoTime());
		Customer cust = em.find(Customer.class, customer.getId());
		cust.setCity(customer.getCity());
		cust.setName(customer.getName());
		cust.setPhone(customer.getPhone());
		cust.setStreet(customer.getStreet());
		cust.setZipCode(customer.getZipCode());
	}

	public void createOrder(Long customerId, Long equipmentId) {
		Logger.log("EnterpriseCustomersBean.createOrder(" + customerId + "," + equipmentId + ")." + System.nanoTime());
		Customer customer = get(customerId);
		Equipment equipment = eqEjb.get(equipmentId);
		Order order = new Order();
		order.setCustomer(customer);
		order.setEquipment(equipment);
		order.setStartDate(new Date());
		em.persist(order);
		equipment.setAvailable(false);
	}

	public void finishOrder(Long orderId) {
		Logger.log("EnterpriseCustomersBean.finishOrder(" + orderId + ")." + System.nanoTime());
		Order order = em.find(Order.class, orderId);
		order.setEndDate(new Date());
		order.getEquipment().setAvailable(true);
	}

	public List<Order> getUnfinishedOrders(Long id) {
		Logger.log("EnterpriseCustomersBean.getUnfinishedOrders(" + id + ")." + System.nanoTime());
		return em.createQuery(
				"from Order o join fetch o.customer c where o.endDate = null and c.id=:custId order by o.id asc",
				Order.class).setParameter("custId", id).getResultList();
	}

	public List<Order> getOrders(Long custId) {
		Logger.log("EnterpriseCustomersBean.getOrders(" + custId + ")." + System.nanoTime());
		if (custId == null) {
			return em.createQuery("from Order o join fetch o.customer c order by o.id desc", Order.class)
					.getResultList();
		} else {
			return em.createQuery("from Order o join fetch o.customer c where c.id = :custId order by o.id desc",
					Order.class).setParameter("custId", custId).getResultList();
		}
	}

	public Order getOrder(Long id) {
		Logger.log("EnterpriseCustomersBean.getOrder(" + id + ")." + System.nanoTime());
		return em.find(Order.class, id);
	}

	public void updateOrder(Long id, Long customerId, Long equipmentId) {
		Logger.log("EnterpriseCustomersBean.updateOrder(" + id + "," + customerId + "," + equipmentId + ")."
				+ System.nanoTime());
		Order order = getOrder(id);
		if (!order.getEquipment().getId().equals(equipmentId)) {
			order.getEquipment().setAvailable(true);
			Equipment equipment = eqEjb.get(equipmentId);
			order.setEquipment(equipment);
			equipment.setAvailable(false);
		}
		Customer customer = get(customerId);
		order.setCustomer(customer);
		em.persist(order);
	}
}
