package pl.edu.pwr.student200275.jsf;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.model.SelectItem;
import javax.inject.Inject;

import pl.edu.pwr.student200275.ejb.EnterpriseEquipmentBean;
import pl.edu.pwr.student200275.jpa.Equipment;

@ManagedBean
@RequestScoped
public class FacesEquipmentBean implements Serializable {
	private static final long serialVersionUID = 6485643313093259275L;

	@Inject
	private EnterpriseEquipmentBean ejb;

	private Long id;
	private String action;
	private Equipment equipment;

	public String save() {
		if (equipment.getId() == null) {
			id = ejb.add(equipment).getId();
		} else {
			ejb.update(equipment);
		}
		return "equipment.xhtml?faces-redirect=true&action=view&id=" + id;
	}

	public boolean isViewAction() {
		return "view".equals(action) && id != null;
	}

	public boolean isDisplayForm() {
		return ("edit".equals(action) && id != null) || "add".equals(action);
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public Collection<Equipment> getEquipmentList() {
		return ejb.getList();
	}
	
	public List<SelectItem> getAvailableEquipment(){
		return ejb.getAvailable().stream().map(eq->{
			return new SelectItem(eq.getId(), eq.getName());
		}).collect(Collectors.toList());
	}

	public List<SelectItem> getEquipmentSelectItemList(){
		return ejb.getList().stream().map(eq->{
			return new SelectItem(eq.getId(), eq.getName(), null, !eq.isAvailable());
		}).collect(Collectors.toList());
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Equipment getEquipment() {
		loadEquipment();
		return equipment;
	}

	public void setEquipment(Equipment equipment) {
		this.equipment = equipment;
	}

	private void loadEquipment() {
		if (equipment == null) {
			if (id != null)
				equipment = ejb.get(id);
			else
				equipment = new Equipment();
		}
	}
}
