package pl.edu.pwr.student200275.jsf;

import java.io.Serializable;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.inject.Inject;

import pl.edu.pwr.student200275.ejb.DbFiller;
import pl.edu.pwr.student200275.ejb.Logger;

@ManagedBean
@SessionScoped
public class FacesEmptyBean implements Serializable {
	private static final long serialVersionUID = -4337412669831457133L;

	@Inject
	DbFiller ejb;
	
	public String getHelloWorld() {
		Logger.log("FacesEmptyBean.helloWorld()." + System.nanoTime());
		return ejb.helloWorld();
	}

}
