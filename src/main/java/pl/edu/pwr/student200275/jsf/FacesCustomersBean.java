package pl.edu.pwr.student200275.jsf;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.model.SelectItem;
import javax.inject.Inject;

import pl.edu.pwr.student200275.ejb.EnterpriseCustomersBean;
import pl.edu.pwr.student200275.ejb.Logger;
import pl.edu.pwr.student200275.jpa.Customer;

@ManagedBean
@RequestScoped
public class FacesCustomersBean implements Serializable {
	private static final long serialVersionUID = 5595881483859585881L;

	@Inject
	private EnterpriseCustomersBean ejb;

	private Long id;
	private Long equipmentId;
	private Long orderId;
	private String action;
	private Customer customer;
	private String customerName;
	private String customerStreet;
	private String customerCity;
	private String customerPhone;
	private String customerZipCode;

	public String save() {
		Logger.log("FacesCustomersBean.save()." + System.nanoTime());
		if (customer.getId() == null) {
			id = ejb.add(customer).getId();
		} else {
			customer.setName(customerName);
			customer.setPhone(customerPhone);
			customer.setCity(customerCity);
			customer.setZipCode(customerZipCode);
			customer.setStreet(customerStreet);
			ejb.update(customer);
		}
		return "customers.xhtml?faces-redirect=true&action=view&id=" + id;
	}

	public String saveOrder() {
		Logger.log("FacesCustomersBean.saveOrder()." + System.nanoTime());
		ejb.createOrder(id, equipmentId);
		return "customers.xhtml?faces-redirect=true&action=view&id=" + id;
	}

	public String finishOrder() {
		Logger.log("FacesCustomersBean.finishOrder()." + System.nanoTime());
		if (orderId != null)
			ejb.finishOrder(orderId);
		return "customers.xhtml?faces-redirect=true&action=view&id=" + id;
	}

	public List<SelectItem> getUnfinishedOrders() {
		Logger.log("FacesCustomersBean.getUnfinishedOrders()." + System.nanoTime());
		return ejb.getUnfinishedOrders(id).stream().map(o -> {
			return new SelectItem(o.getId(), o.getEquipment().getName());
		}).collect(Collectors.toList());
	}

	public boolean isViewAction() {
		return ("newOrder".equals(action) || "finishOrder".equals(action) || "view".equals(action)) && id != null;
	}

	public boolean isDisplayForm() {
		return ("edit".equals(action) && id != null) || "add".equals(action);
	}

	public boolean isDisplayOrderForm() {
		return "newOrder".equals(action);
	}

	public boolean isDisplayDetailsSection() {
		return id != null || "add".equals(action);
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public Collection<Customer> getCustomers() {
		Logger.log("FacesCustomersBean.getCustomers()." + System.nanoTime());
		List<Customer> list = ejb.getList();
		return list;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Customer getCustomer() {
		loadCustomer();
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	public Long getEquipmentId() {
		return equipmentId;
	}

	public void setEquipmentId(Long equipmentId) {
		this.equipmentId = equipmentId;
	}

	private void loadCustomer() {
		Logger.log("FacesCustomersBean.loadCustomer()." + System.nanoTime());
		customer = ejb.get(id);
		customerName = customer.getName();
		customerCity = customer.getCity();
		customerPhone = customer.getPhone();
		customerStreet = customer.getStreet();
		customerZipCode = customer.getZipCode();
	}

	public Long getOrderId() {
		return orderId;
	}

	public void setOrderId(Long orderId) {
		this.orderId = orderId;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getCustomerStreet() {
		return customerStreet;
	}

	public void setCustomerStreet(String customerStreet) {
		this.customerStreet = customerStreet;
	}

	public String getCustomerCity() {
		return customerCity;
	}

	public void setCustomerCity(String customerCity) {
		this.customerCity = customerCity;
	}

	public String getCustomerPhone() {
		return customerPhone;
	}

	public void setCustomerPhone(String customerPhone) {
		this.customerPhone = customerPhone;
	}

	public String getCustomerZipCode() {
		return customerZipCode;
	}

	public void setCustomerZipCode(String customerZipCode) {
		this.customerZipCode = customerZipCode;
	}

	public List<SelectItem> getCustomersSelectItemList() {
		Logger.log("FacesCustomersBean.getCustomersSelectItemList()." + System.nanoTime());
		return ejb.getList().stream().map(cust -> {
			return new SelectItem(cust.getId(), cust.getName());
		}).collect(Collectors.toList());
	}
}
