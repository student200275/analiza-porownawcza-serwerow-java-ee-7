package pl.edu.pwr.student200275.jsf;

import java.io.Serializable;
import java.util.Date;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.inject.Inject;

import pl.edu.pwr.student200275.ejb.EnterpriseCustomersBean;
import pl.edu.pwr.student200275.ejb.Logger;
import pl.edu.pwr.student200275.jpa.Order;

@ManagedBean
@RequestScoped
public class FacesOrderBean implements Serializable{
	private static final long serialVersionUID = -8928059076554502265L;

	@Inject
	private EnterpriseCustomersBean ejb;

	private boolean loaded = false;
	private Long id;
	private Date startDate;
	private Date endDate;
	private Long customerId;
	private String customerName;
	private Long equipmentId;
	private String equipmentName;
	
	public String save() {
		if (id == null) {
			ejb.createOrder(customerId, equipmentId);
		} else {
			ejb.updateOrder(id, customerId, equipmentId);
		}
		return "customer.xhtml?faces-redirect=true&id=" + customerId;
	}
	
	public Long getCustomerId() {
		loadOrder();
		return customerId;
	}

	public void setCustomerId(Long customerId) {
		this.customerId = customerId;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getEquipmentId() {
		loadOrder();
		return equipmentId;
	}

	private void loadOrder() {
		Logger.log("FacesOrderBean.loadOrder()." + System.nanoTime());
		if (id != null && !loaded) {
			Order order = ejb.getOrder(id);
			this.startDate = order.getStartDate();
			this.endDate = order.getEndDate();
			this.customerId = order.getCustomer().getId();
			this.customerName = order.getCustomer().getName();
			this.equipmentId = order.getEquipment().getId();
			this.equipmentName = order.getEquipment().getName();
			loaded = true;
		}
	}
	
	public String finish(Long id) {
		ejb.finishOrder(id);
		return "order.xhtml?faces-redirect=true&id=" + id;
	}

	public void setEquipmentId(Long equipmentId) {
		this.equipmentId = equipmentId;
	}

	public Date getStartDate() {
		loadOrder();
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		loadOrder();
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public String getCustomerName() {
		loadOrder();
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getEquipmentName() {
		loadOrder();
		return equipmentName;
	}

	public void setEquipmentName(String equipmentName) {
		this.equipmentName = equipmentName;
	}
	
}
