package pl.edu.pwr.student200275.jsf;

import java.io.Serializable;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.inject.Inject;

import pl.edu.pwr.student200275.ejb.EnterpriseCustomersBean;
import pl.edu.pwr.student200275.ejb.Logger;
import pl.edu.pwr.student200275.jpa.Customer;

@ManagedBean
@RequestScoped
public class FacesCustomerBean implements Serializable{
	private static final long serialVersionUID = 7803202834533286888L;

	@Inject
	private EnterpriseCustomersBean ejb;

	private Long id;
	private String customerName;
	private String customerStreet;
	private String customerCity;
	private String customerPhone;
	private String customerZipCode;

	private void loadCustomer() {
		if (customerName == null && id != null) {
			Logger.log("FacesCustomerBean.getCustomer()." + System.nanoTime());
			Customer customer = ejb.get(id);
			customerName = customer.getName();
			customerCity = customer.getCity();
			customerPhone = customer.getPhone();
			customerStreet = customer.getStreet();
			customerZipCode = customer.getZipCode();
			Logger.log("FacesCustomerBean.getCustomer()." + System.nanoTime());
		}
	}

	public String save() {
		if (id != null) {
			Logger.log("FacesCustomerBean.save()." + System.nanoTime());
			Customer cust = new Customer();
			cust.setId(id);
			cust.setName(customerName);
			cust.setCity(customerCity);
			cust.setPhone(customerPhone);
			cust.setZipCode(customerZipCode);
			cust.setStreet(customerStreet);
			ejb.update(cust);
			return "customer.xhtml?faces-redirect=true&id=" + id;
		}
		return null;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCustomerName() {
		loadCustomer();
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getCustomerStreet() {
		loadCustomer();
		return customerStreet;
	}

	public void setCustomerStreet(String customerStreet) {
		this.customerStreet = customerStreet;
	}

	public String getCustomerCity() {
		loadCustomer();
		return customerCity;
	}

	public void setCustomerCity(String customerCity) {
		this.customerCity = customerCity;
	}

	public String getCustomerPhone() {
		loadCustomer();
		return customerPhone;
	}

	public void setCustomerPhone(String customerPhone) {
		this.customerPhone = customerPhone;
	}

	public String getCustomerZipCode() {
		loadCustomer();
		return customerZipCode;
	}

	public void setCustomerZipCode(String customerZipCode) {
		this.customerZipCode = customerZipCode;
	}

}
