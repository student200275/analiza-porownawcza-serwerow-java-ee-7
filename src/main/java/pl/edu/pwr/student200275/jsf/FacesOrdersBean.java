package pl.edu.pwr.student200275.jsf;

import java.io.Serializable;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.inject.Inject;

import pl.edu.pwr.student200275.ejb.EnterpriseCustomersBean;
import pl.edu.pwr.student200275.jpa.Order;

@ManagedBean
@RequestScoped
public class FacesOrdersBean implements Serializable{
	private static final long serialVersionUID = 4043962227521336598L;

	@Inject
	private EnterpriseCustomersBean ejb;

	private Long customerId;
	
	public List<Order> getOrders(){
		return ejb.getOrders(customerId); 
	}

	public Long getCustomerId() {
		return customerId;
	}

	public void setCustomerId(Long customerId) {
		this.customerId = customerId;
	}
	
}
