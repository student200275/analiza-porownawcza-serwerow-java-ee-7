package pl.edu.pwr.student200275.jaxrs;

import java.io.Serializable;
import java.util.List;

import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import pl.edu.pwr.student200275.ejb.EjbStateful;
import pl.edu.pwr.student200275.ejb.EnterpriseCustomersBean;
import pl.edu.pwr.student200275.jpa.Customer;

@Path("/sessionScoped")
@SessionScoped
@Produces(MediaType.APPLICATION_JSON)
public class EndpointSessionScoped implements Serializable {
	private static final long serialVersionUID = 3370015180638271973L;

	@GET
	@Path("/helloWorld")
	public String helloWorld() {
		return "Hello, world!";
	}
	
	@Inject
	EjbStateful ejb;

	@GET
	@Path("/ejb/helloWorld")
	public String ejbHelloWorld() {
		return ejb.helloWorld();
	}
	
	@Inject
	EnterpriseCustomersBean customersBean;
	
	@GET
	@Path("/customers")
	public List<Customer> getList(){
		return customersBean.getList();
	}
}
