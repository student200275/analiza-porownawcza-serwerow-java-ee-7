package pl.edu.pwr.student200275.jaxrs;

import java.io.Serializable;

import javax.ws.rs.GET;
import javax.ws.rs.Path;

import pl.edu.pwr.student200275.ejb.Logger;

@Path("/")
public class EndpointNotManaged implements Serializable {
	private static final long serialVersionUID = 3646849558430251525L;

	@GET
	@Path("/helloWorld")
	public String helloWorld() {
		return "Hello, world!";
	}
	
	@GET
	@Path("/log")
	public void log() {
		Logger.print();
	}

}
